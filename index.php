<?php

$file =  __DIR__. '/vendor/autoload.php';

if (!file_exists($file)) { 
    exit;
}

require_once $file;