<?php

namespace Lableb\Exceptions;

use Exception as CoreException;

/**
 * Class Exception
 *
 * @author  Lableb Team  <support@lableb.com>
 */
class Exception extends CoreException
{

}

