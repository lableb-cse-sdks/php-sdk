<?php

namespace Lableb\ServiceProviders;

use Illuminate\Support\ServiceProvider;
use Lableb\Contracts\LablebInterface;
use Lableb\Facades\LablebFacadeAccessor;
use Lableb\LablebSDK;

/**
 * Class LablebServiceProvider
 *
 * @author  Lableb Team  <support@lableb.com>
 */
class LablebServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the package.
     */
    public function boot()
    {
        /*
        |--------------------------------------------------------------------------
        | Publish the Config file from the Package to the App directory
        |--------------------------------------------------------------------------
        */
        $this->configPublisher();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        /*
        |--------------------------------------------------------------------------
        | Implementation Bindings
        |--------------------------------------------------------------------------
        */
        $this->implementationBindings();

        /*
        |--------------------------------------------------------------------------
        | Facade Bindings
        |--------------------------------------------------------------------------
        */
        $this->facadeBindings();

        /*
        |--------------------------------------------------------------------------
        | Registering Service Providers
        |--------------------------------------------------------------------------
        */
        $this->serviceProviders();
    }

    /**
     * Implementation Bindings
     */
    private function implementationBindings()
    {
        $this->app->bind(
            LablebInterface::class,
            LablebSDK::class
        );
    }

    /**
     * Publish the Config file from the Package to the App directory
     */
    private function configPublisher()
    {
        // When users execute Laravel's vendor:publish command, the config file will be copied to the specified location
        $this->publishes([
            dirname(__DIR__)  . '/Config/lableb.php' => config_path('lableb.php'),
        ]); 
    }

    /**
     * Facades Binding
     */
    private function facadeBindings()
    {
        $this->app->bind('lableb', LablebSDK::class);
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('LablebSDK', LablebFacadeAccessor::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    /**
     * Registering Other Custom Service Providers (if you have)
     */
    private function serviceProviders()
    {
        // $this->app->register('...\...\...');
    }

}
