<?php

return [

    /*
    |--------------------------------------------------------------------------
    |
    |--------------------------------------------------------------------------
    |
    */
    'project_name' =>  env('LABLEB_PROJECT_NAME'),
    'search_api_key' =>  env('LABLEB_SEARCH_API_KEY'),
    'index_api_key' =>  env('LABLEB_INDEX_API_KEY'),
];
