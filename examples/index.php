<?php

/* Based On  https://www.php.net/manual/en/features.commandline.webserver.php
Warning 
    This web server is designed to aid application development. 
    It may also be useful for testing purposes or for application demonstrations that are run in controlled environments. 
    It is not intended to be a full-featured web server. It should not be used on a public network.
Warning
    The built-in Web Server should not be used on a public network.
>>> php -S localhost:8000

*/
$file =  dirname(__DIR__) . '/vendor/autoload.php';

if (!file_exists($file)) {
    exit;
}

require_once $file;
require_once  dirname(__DIR__).'/env.php';

use Lableb\LablebSDK;

$lableb = new LablebSDK(
    getenv('PROJECT_NAME'),
    getenv('SEARCH_API_KEY'),
    getenv('INDEXING_API_KEY')
);

date_default_timezone_set('Europe/Istanbul');

$indexName = 'index';
$documents = [[
      "id" => 1,
      "title" => "title 1",
      "content" => "this is article content",
      "category" => ["cat1", "cat2"],
      "tags" => ["tag1", "tag2"],
      "url" => "https://solutions.lableb.com/en/doc/php-sdk/index-documents",
      "authors" => ["Lableb Team"],
      "date" => new \DateTime(),
  ], [
      "id" => 2,
      "title" => "title 2",
      "content" => "this is article content",
      "category" => ["cat1", "cat2"],
      "tags" => ["tag1", "tag2"],
      "url" => "https://solutions.lableb.com/en/doc/php-sdk/index-documents",
      "authors" => ["Lableb Team"],
      "date" => new \DateTime(),
  ], [
      "id" => 3,
      "title" => "title 3",
      "content" => "this is article content",
      "category" => ["cat3"],
      "tags" => ["tag3", "tag4"],
      "url" => "https://solutions.lableb.com/en/doc/php-sdk/index-documents",
      "authors" => ["Lableb Team"],
      "date" => new \DateTime(),
  ], [
      "id" => 4,
      "title" => "title 4",
      "content" => "this is article content",
      "category" => ["cat4"],
      "tags" => ["tag4"],
      "url" => "https://solutions.lableb.com/en/doc/php-sdk/index-documents",
      "authors" => ["Lableb Team"],
      "date" => new \DateTime(),
  ], [
      "id" => 5,
      "title" => "title 5",
      "content" => "this is article content",
      "category" => ["cat5"],
      "tags" => ["tag5"],
      "url" => "https://solutions.lableb.com/en/doc/php-sdk/index-documents",
      "authors" => ["Lableb Team"],
      "date" => new \DateTime(),
  ], [
      "id" => 6,
      "title" => "title 6",
      "content" => "this is article content",
      "category" => ["cat6"],
      "tags" => ["tag6"],
      "url" => "https://solutions.lableb.com/en/doc/php-sdk/index-documents",
      "authors" => ["Lableb Team -Bashar Modallal"],
      "date" => new \DateTime(),
  ]
];
$autocomplete_query = [
    'q' => 'titl',
    'limit'=>2,
];
$search_query = [
    'q' => 'titl',
    'filter' => [
        'category' => 'cat2',
        'tags' => [ 'tag1', 'tag2' ],
    ],
    'author' => 'Lableb Team',
    'skip' =>0,
    'limit'=>2,
    'sort' =>'date asc'
];

$recommend_query = [
    'id' => 3,
    'limit'=>5
];

$feedback_query = [
    "query"=> "سامسونج",
    "item_id"=> "1",
];

$source_feedback_query = [
    "query"=> "سامسونج",
    "id"=> "1",
    "user_id"=> "1",
    "user_ip"=> "167.114.64.183",
    "url"=> "https://RLOCME0425.expandcart.com/product/product&product_id=129",
    "title"=> "LG 55 inch LCD HDTV - TV with 4K in",
    "country"=>'damas'
];

$target_feedback_query = [
    'id' => 2,
    'item_order'=>"2",
    "url"=> "https://RLOCME0425.expandcart.com/product/product&product_id=130",
    "title"=> "LG 601 inch LCD HDTV - TV with 2K in"
];

$id=2;

echo '<div style="width:70%;margin:auto;overflow-x:scroll;">';

#Indexing Example

try
{ 
    $response = $lableb->index($indexName,$documents);
    
    echo '<h1>index</h1><pre style="background-color:#85C1E9;">';
      print_r($response);
    echo '</pre>';
} catch (\Lableb\Exceptions\LablebException $e) {
    echo $e->getStatus() . " - " . $e->getMessage();
}

#Search Example
try
{
    $response = $lableb->search($indexName,$search_query );
    echo '<h1>search</h1><pre style="background-color:#85C1E9;">';
      print_r($response);
    echo '</pre>';
     
} catch (\Lableb\Exceptions\LablebException $e) {
    echo $e->getStatus() . " - " . $e->getMessage();
}

#Autocomplete Example
try
{
    $response = $lableb->autocomplete($indexName,$autocomplete_query );
    
    echo '<h1>autocomplete</h1><pre style="background-color:#5DADE2;">';
      print_r($response);
    echo '</pre>';
     
} catch (\Lableb\Exceptions\LablebException $e) {
    echo $e->getStatus() . " - " . $e->getMessage();
}

#Recommend Example
try
{
    $response = $lableb->recommend($indexName,$recommend_query );

    echo '<h1>recommend</h1><pre style="background-color:#3498DB;">';
      print_r($response);
    echo '</pre>';
     
} catch (\Lableb\Exceptions\LablebException $e) {
    echo $e->getStatus() . " - " . $e->getMessage();
}


#Search Feedback Example
try
{
    $response = $lableb->submitSearchFeedback($indexName,$feedback_query );
    
    echo '<h1>submitSearchFeedback</h1><pre style="background-color:#2E86C1;">';
      print_r($response);
    echo '</pre>';
     
} catch (\Lableb\Exceptions\LablebException $e) {
    echo $e->getStatus() . " - " . $e->getMessage();
}


#Autocomplete Feedback Example
try
{
    $response = $lableb->submitAutocompleteFeedback($indexName,$feedback_query );
    
    echo '<h1>submitAutocompleteFeedback</h1><pre style="background-color:#21618C;">';
      print_r($response);
    echo '</pre>';
     
} catch (\Lableb\Exceptions\LablebException $e) {
    echo $e->getStatus() . " - " . $e->getMessage();
}

#Recommend Feedback Example
try
{
    $response = $lableb->submitRecommendationFeedback($indexName,$source_feedback_query ,$target_feedback_query);
    
    echo '<h1>submitRecommendationFeedback</h1><pre style="background-color:#1B4F72;">';
      print_r($response);
    echo '</pre>';
     
} catch (\Lableb\Exceptions\LablebException $e) {
    echo $e->getStatus() . " - " . $e->getMessage();
}

#Delete Example
try
{
    $response = $lableb->delete($indexName,$id );
    
    echo '<h1>delete</h1><pre style="background-color:#5DADE2;">';
      print_r($response);
    echo '</pre>';
     
} catch (\Lableb\Exceptions\LablebException $e) {
    echo $e->getStatus() . " - " . $e->getMessage();
}
echo '</div>';
